output "cluster_endpoint" {
  description = "Endpoint par o Control Plane do EKS"
  value       = module.eks.cluster_endpoint
}

output "cluster_security_group_id" {
  description = "SGs Control Plane"
  value       = module.eks.cluster_security_group_id
}

output "kubectl_config" {
  description = "Kubeconfig para contexto"
  value       = module.eks.kubeconfig
}

output "config_map_aws_auth" {
  description = "Configmap para autenticação"
  value       = module.eks.config_map_aws_auth
}

output "region" {
  description = "AWS region"
  value       = var.region
}

output "cluster_name" {
  description = "Nome do cluster EKS"
  value       = local.cluster_name
}
