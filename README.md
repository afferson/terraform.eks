# PROVISIONANDO UM CLUSTER EKS COM TERRAFORM

Receita do Terraform criada para provisionar um cluster isolado do EKS na AWS

## Componentes que são criados:

- AWS VPC
- Public Subnet AZ 1
- Public Subnet AZ 2
- Public Subnet AZ 3
- Private Subnet AZ 1
- Private Subnet AZ 2
- Private Subnet AZ 3
- Internet Gatway
- Route Table
- IAM Functions
- Auto Scaling Group
- EKS CLUSTER

## Instalar modulos necessários para esta configuração

> ~$ terraform init

## Executando plano do Terraform

> ~$ terraform plan 

## Provisiando  o ambiente

> ~$ terraform apply

## Configurar kubectl

> ~$ aws eks --region us-east-2 update-kubeconfig --name terraform-eks-poc

## Instalar Metrics Server

> ~$ wget -O v0.3.6.tar.gz https://codeload.github.com/kubernetes-sigs/metrics-server/tar.gz/v0.3.6 && tar -xzf v0.3.6.tar.gz
> ~$ kubectl apply -f metrics-server-0.3.6/deploy/1.8+/

## Instalação do Helm (.kube)

> ~$ kubectl apply -f tiller-user.yaml
> ~$ helm init --service-account tiller

# Instalalão do Ingress

...
helm install \
stable/nginx-ingress \
--name my-nginx \
--set rbac.create=true
...

## Destruindo o ambiente

> ~$ helm del --purge nginx-ingress
> ~$ terraform destroy



### (https://www.terraform.io/docs/providers/http/index.html)
